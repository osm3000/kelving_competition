# Regularization
The objective here is for me to review what is regularization in deep learning, since I don't remember shit about it.

## Possible ways to do regularization
1. More data
2. L1 and L2
3. [Model layers](https://keras.io/api/layers/#regularization-layers)
   1. Dropout
      1. Uniform
      3. Gaussian
      2. Spatial
         1. It drops entire feature maps
   2. Noise layer