from exp_2 import preprocessing
import pandas as pd
import numpy as np
import pygmo as pg
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
sequences_ids = np.random.choice(np.arange(1, 1281), size=800)
features, labels, _ = preprocessing.generate_dataset(
    sequence_id_list=sequences_ids, path='./data')

labels_needed = labels <= 3
labels = labels[labels_needed]
features = features[labels_needed]
features = features.reshape(features.shape[0], -1)
# features = np.digitize(features, [0, .2, .4, .6, .8, 1])
# print(train_X.shape)
# print(train_X[0].tolist())
# print(np.unique(train_X))
# print(np.bincount(train_X.flatten()))

###############################
# Compare this to uniform quantization distribution
train_X, val_X, train_y, val_y = train_test_split(
    features, labels, test_size=0.1, shuffle=True, stratify=labels, random_state=0)

classifier_model = RandomForestClassifier(n_estimators=10, n_jobs=8)
classifier_model.fit(train_X, train_y)
score = classifier_model.score(val_X, val_y)
print(f'No quantization score: {score}')
del train_X
del train_y
del val_X
del val_y
del classifier_model

###############################
# Compare this to uniform quantization distribution
NB_LEVELS = 8
levels = np.linspace(0, 1, NB_LEVELS)
print(f'BASELINE levels: {levels}')
levels = np.sort(levels)
features_new = np.digitize(features, levels) / NB_LEVELS

train_X, val_X, train_y, val_y = train_test_split(
    features_new, labels, test_size=0.1, shuffle=True, stratify=labels, random_state=0)

classifier_model = RandomForestClassifier(n_estimators=10, n_jobs=8)
classifier_model.fit(train_X, train_y)
score = classifier_model.score(val_X, val_y)
print(f'BASELINE score: {score}')
del features_new
del  train_X
del  train_y
del val_X
del val_y
del classifier_model

class quantize_data:
    def __init__(self, nb_levels=8):
        super().__init__()
        self.dim = nb_levels
    
    def fitness(self, levels):
        global features
        global labels
        levels = np.sort(levels)
        features_new = np.digitize(features, levels) / self.dim

        train_X, val_X, train_y, val_y = train_test_split(
            features_new, labels, test_size=0.1, shuffle=True, stratify=labels, random_state=0)

        classifier_model = RandomForestClassifier(n_estimators=10, n_jobs=8)

        classifier_model.fit(train_X, train_y)

        score = classifier_model.score(val_X, val_y)
        # score = f1_score(val_y, classifier_model.predict(val_X), average='weighted')
        # print(f'score: {score}')
        # return np.array([-score]).astype(np.float32)
        del features_new
        del train_X
        del train_y
        del val_X
        del val_y
        del classifier_model
        return [-score,]

    def get_bounds(self):
        return ([0] * self.dim, [1] * self.dim)

    def get_name(self):
        return f"Quantization problems - # of levels: {self.dim}"


algo = pg.algorithm(pg.sga(gen=1000))
algo.set_verbosity(5)
pop = pg.population(quantize_data(nb_levels=8), 10)
pop = algo.evolve(pop)

print("Best solution: ", pop.champion_x)
print("Best score: ", pop.champion_f)
