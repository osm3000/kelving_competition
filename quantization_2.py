'''
Instead of using a classification score as a fitness, I will use the re-construction error
'''
from exp_2 import preprocessing
import pandas as pd
import numpy as np
import pygmo as pg
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
from numba import jit, njit

sequences_ids = np.random.choice(np.arange(1, 1281), size=500)
features, labels, _ = preprocessing.generate_dataset(
    sequence_id_list=sequences_ids, path='./data')

features = features.astype(np.float32)
# labels_needed = labels <= 3
# labels = labels[labels_needed]
# features = features[labels_needed]
features = features.reshape(features.shape[0], -1)

###############################
# No quantization at all
# train_X, val_X, train_y, val_y = train_test_split(
#     features, labels, test_size=0.1, shuffle=True, stratify=labels, random_state=0)

# classifier_model = RandomForestClassifier(
#     n_estimators=10, n_jobs=8, random_state=0)
# classifier_model.fit(train_X, train_y)
# score = classifier_model.score(val_X, val_y)
# print(f'No quantization score: {score}')
# del train_X
# del train_y
# del val_X
# del val_y
# del classifier_model

###############################
# Compare this to uniform quantization distribution
NB_LEVELS = 8
levels = np.linspace(0, 1, NB_LEVELS)
print(f'BASELINE levels: {levels}')
levels = np.sort(levels)
features_new = np.digitize(features, levels) / NB_LEVELS

score = np.mean((features_new - features)**2)
print(f'BASELINE reconstruction score: {score}')

# train_X, val_X, train_y, val_y = train_test_split(
#     features_new, labels, test_size=0.1, shuffle=True, stratify=labels, random_state=0)

# classifier_model = RandomForestClassifier(
#     n_estimators=10, n_jobs=8, random_state=0)
# classifier_model.fit(train_X, train_y)
# score = classifier_model.score(val_X, val_y)
# print(f'BASELINE score: {score}')
# del features_new
# del  train_X
# del  train_y
# del val_X
# del val_y
# del classifier_model

##################################
# Best quantization based on the reconstruction error
# [0.32124243,0.56026856,0.6945468,0.44033507,0.9636294,0.82367583,0.06387491,0.18990075]
# levels = [0.32124243, 0.56026856, 0.6945468, 0.44033507,
#           0.9636294, 0.82367583, 0.06387491, 0.18990075]
# levels = np.sort(levels)
# features_new = np.digitize(features, levels) / NB_LEVELS

# score = np.mean((features_new - features)**2)
# print(f'BEST reconstruction score: {score}')

# train_X, val_X, train_y, val_y = train_test_split(
#     features_new, labels, test_size=0.1, shuffle=True, stratify=labels, random_state=0)

# classifier_model = RandomForestClassifier(n_estimators=10, n_jobs=8, random_state=0)
# classifier_model.fit(train_X, train_y)
# score = classifier_model.score(val_X, val_y)
# print(f'Best quantization score: {score}')
# del features_new
# del train_X
# del train_y
# del val_X
# del val_y
# del classifier_model
# exit()


@jit(nopython=True, parallel=True, cache=True)
def compute_reconstruction(levels, features):
    # global features
    # global labels
    levels = np.sort(levels).astype(np.float32)
    features_new = np.digitize(features, levels) / len(levels)
    score = np.mean((features_new - features)**2)
    # del features_new
    return score

class quantize_data:
    def __init__(self, nb_levels=8):
        super().__init__()
        self.dim = nb_levels
    
    def fitness(self, levels):
        global features
        # given_features = np.random.choice(features, size=20,)
        _, given_features = train_test_split(features, test_size=0.1, shuffle=True)
        # score = compute_reconstruction(levels, features)
        score = compute_reconstruction(levels, given_features)
        print(f'score: {score}')
        return [score,]

    def get_bounds(self):
        return ([0] * self.dim, [1] * self.dim)

    def get_name(self):
        return f"Quantization problems - # of levels: {self.dim}"


algo = pg.algorithm(pg.sea(gen=1000))
algo.set_verbosity(5)
pop = pg.population(quantize_data(nb_levels=NB_LEVELS), 20)
pop = algo.evolve(pop)

print("Best solution: ", pop.champion_x)
print("Best score: ", pop.champion_f)
