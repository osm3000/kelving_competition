from preprocessing import generate_dataset
import numpy as np
import matplotlib as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import numpy.ma as ma

if __name__ == "__main__":
    sats_coord, nb_of_sats, features = generate_dataset(sequence_id_list=range(1, 100), path="../data", max_num_of_sats=3, padding_value=-1)
    sats_coord = sats_coord.astype(np.int16)

    features = features.reshape(features.shape[0], -1)
    sats_coord = sats_coord.reshape(sats_coord.shape[0], -1)
    mask_value = sats_coord == -1
    masked_sats_coord = ma.array(sats_coord, mask=mask_value)
    # print(sats_coord)
    # print(masked_sats_coord)
    # exit()
    
    # Standard scaling both inputs and outputs
    features = MinMaxScaler(feature_range=(0, 1)).fit_transform(features)
    # sats_coord = MinMaxScaler(feature_range=(0, 1)).fit_transform(sats_coord)
    masked_sats_coord = MinMaxScaler(feature_range=(0, 1)).fit_transform(masked_sats_coord)
    masked_sats_coord = ma.array(masked_sats_coord, mask=mask_value)

    print(np.min(features), np.max(features))
    print(np.min(masked_sats_coord), np.max(masked_sats_coord))
    print(type(masked_sats_coord))

    # exit()
    # sats_coord_train, sats_coord_test, nb_of_sats_train, nb_of_sats_test, features_train, features_test = train_test_split(sats_coord, nb_of_sats, features, test_size=0.1)
    sats_coord_train, sats_coord_test, nb_of_sats_train, nb_of_sats_test, features_train, features_test = train_test_split(masked_sats_coord, nb_of_sats, features, test_size=0.1)

    model = RandomForestRegressor(n_jobs=10, n_estimators=50, verbose=10, max_depth=30)
    
    model.fit(features_train, sats_coord_train)

    train_prediction = model.predict(features_train)
    train_r2_score = r2_score(sats_coord_train, train_prediction)
    train_mse_score = mean_squared_error(sats_coord_train, train_prediction)

    print(f'Train --> MSE: {train_mse_score} \t\t R2: {train_r2_score}')

    test_prediction = model.predict(features_test)
    test_r2_score = r2_score(sats_coord_test, test_prediction)
    test_mse_score = mean_squared_error(sats_coord_test, test_prediction)

    print(f'Test --> MSE: {test_mse_score} \t\t R2: {test_r2_score}')
