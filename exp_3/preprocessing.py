from skimage import measure
import random
import itertools
import json
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
import pickle
random.seed(0)

class PreProcessing():
    def __init__(self):
        pass

    @staticmethod
    def read_image(path):
        return plt.imread(path)

    @staticmethod
    def generate_labeled_data(image_path, annotation):
        im_array = PreProcessing.read_image(image_path)
        nb_objects = len(annotation)

        return nb_objects, im_array

    @staticmethod
    def generate_labeled_set(annotation_array, path, sequence_id_list, max_num_of_sats, padding_value):
        # Generate labeled data for a list of sequences in a given path
        labels = []
        features = []
        coordinates = []
        global_index = 0
        for seq_index, seq_id in enumerate(sequence_id_list):
            for frame_id in range(1, 6):
                d = PreProcessing.generate_labeled_data(f"{path}{seq_id}/{frame_id}.png", annotation_array[seq_id][frame_id])
                nb_of_coordinates = d[0]
                
                if nb_of_coordinates <= max_num_of_sats:
                    labels.append(d[0])
                    features.append(d[1].astype(np.float16))
                    if nb_of_coordinates > max_num_of_sats:
                        coordinates.append(annotation_array[seq_id][frame_id][:max_num_of_sats])
                    elif nb_of_coordinates < max_num_of_sats:
                        coordinates.append(annotation_array[seq_id][frame_id] + [[padding_value, padding_value]] * (max_num_of_sats-nb_of_coordinates))
                    else:
                        coordinates.append(annotation_array[seq_id][frame_id])

                global_index += 1
                # print(labels[-1], " --- ", coordinates[-1])
        return np.stack(coordinates), np.array(labels).astype(np.int8), np.stack(features)

    @staticmethod
    def read_annotation_file(path='../spotGEO/train_anno.json'):
        print(f'path: {path}')
        with open(path) as annotation_file:
            annotation_list = json.load(annotation_file)
        # Transform list of annotations into dictionary
        annotation_dict = {}
        for annotation in annotation_list:
            sequence_id = annotation['sequence_id']
            if sequence_id not in annotation_dict:
                annotation_dict[sequence_id] = {}
            annotation_dict[sequence_id][annotation['frame']] = annotation['object_coords']
        return annotation_dict


def generate_dataset(sequence_id_list=range(1, 101), path="../data", max_num_of_sats = 3, padding_value = -1):
    train_annotation = PreProcessing.read_annotation_file(
        f'{path}/train_anno.json')

    sats_coord, nb_of_sats, features = PreProcessing.generate_labeled_set(
        annotation_array=train_annotation, path=f'{path}/train/', sequence_id_list=sequence_id_list, max_num_of_sats=3, padding_value=-1)
    print(
        f'features: {features.shape} -- nb_of_sats: {nb_of_sats.shape} -- sats_coord: {sats_coord.shape}')
    return sats_coord, nb_of_sats, features

if __name__ == "__main__":
    generate_dataset(sequence_id_list=range(1, 20), path="../data", max_num_of_sats=3, padding_value=-1)
