Now, I want to train the regressor part, and use it to find the location of the satellite. I will focus on 3 or less satellites for now as well.

What is expected here is to generate the final submission file, even without the classifier being ready for masking.

# Questions
* When selecting the examples for training, should I completely exclude the examples that has more than N satellites (which is what I am doing right now)? Or should I select them, but truncate the results?
  * Keep this point in mind for now. Let's focus on finishing the whole pipeline for submission first.
* An idea of interest could be to using RNN on the sequence, where we can better make the guesses over the whole sequence, not just one image at a time.
  * It could be good at that time to interpolate the position of the satellites, where it is missing.
* For the classification, to measure the quality of the model (the true quality), we shouldn't punish the model based on the wrong answer, but based on how much far it is from the truth. For that, maybe building a regressor is a better idea, and then latter I apply a ceiling function to convert it to a classifier.
* To standardize the data, while taking the padding into account, we know apriori that the padding is the lowest value, thus, we can normalize/standardize it with the rest of the data, and then select the lowest value (in X and Y separately), and set it again to another low value (like -1) --> If needed

# Comments
* The training time of a simple RF regressor, with 100 estimator, on 1000 sequences (around 4000 frames) is nothing short of .... impressive! 163 min on 10 cores. Now, this is not acceptable. With 10 trees only, it takes 15 min. Now, for fast prototyping, especially at this early stage of the work, this is not going to work.
* I think the use of -1 for the empty places is not a good idea. Is there way to introduce a masking, or "i don't care" notion in the output? 
  * Using masked arrays from numpy doesn't seem to affect the model behavior towards to the masked values. For example, while applying a `MinMaxScaler` transformer on the masked array, the transformer interpolated the missing values, and the result is a normal array, not a masked one. This suggests that the same is happening with the `RF` model. 
    * Maybe we keep the current situation as it is, but when we want to score the final model, we remove the masked values, and compute the score based on the rest only. This will not solve the issue during the training, but at least we can have a good idea about the model's actual performance. 