# Problem description
* Given 5 images for different, but overlapping, consecutive images, detect orbiting objects in the geostationary ring
* The pictures are in PNG format - not the usual FITS format used for astronomy
* Just pictures, no additional meta-data
* Each image/frame is 40 sec exposer
  * Q: will this affect where the satellite is? will it appear as a pixel, or a moving line?
  * Should each image be treated separately? or the sequence of images has a meaning?
    * Maybe it does have a meaning, because, the same satellites will be present in multiple frames.
* > Note that under the adopted capture regime, stars appear as streaks, while GEO or near-GEO objects mostly appear as blobs or shorter streaks since they are (mostly) static relative to the observer.
  * Why?? physically speaking, I don't understand 

---
# Shape of the data

---
# Flow of operation (from the starter kit) 
**This is not necessarily the right idea, but it is a starting point**
This will be a classification problem, which we will manipulate later to make it a regression problem. 

We will take windows from the data, and will label them as having a valid object or not based on the knowledge we have about the precise place of the object (the *json* file).

The data we generate (the windows) should reflect the actual imbalance in the data (we should have fewer positive examples than negative examples).
  * Do we really need to do that? it could be actually a good opportunity to provide balanced data to the classifier

In order to classify a whole image, we move the window over each part of the image, classify it, and report classification value for each pixel (0 or 1). 



---
# Possible alternative approach
* Explore more object recognition (classification, detection, localization) using deep learning (and machine learning in general)