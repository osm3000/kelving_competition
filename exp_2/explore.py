# What is the number of data points per frame?
import json
import matplotlib.pyplot as plt
import numpy as np
import preprocessing
# path = '../spotGEO/train_anno.json' # path for the annotation file
# sizes = []
# with open(path) as annotation_file:
#     annotation_list = json.load(annotation_file)
#     # Transform list of annotations into dictionary
#     annotation_dict = {}
#     for annotation in annotation_list:
#         sequence_id = annotation['sequence_id']
#         if sequence_id not in annotation_dict:
#             annotation_dict[sequence_id] = {}
#         annotation_dict[sequence_id][annotation['frame']] = annotation['object_coords']
#         sizes.append(len(annotation['object_coords']))


# plt.figure()
# plt.title("Distribution of number of objects per frame")
# plt.hist(sizes)
# # plt.show()
# plt.savefig("nb_objects_dist.png")


'''
What is the distribution of light in the images?
'''
# Choose a 100 random sequences
sequences_ids = np.random.choice(np.arange(1, 1281), size=300)
# sequences_ids = np.arange(1, 1281)
print(sequences_ids)
images, _, _ = preprocessing.generate_dataset(sequence_id_list=sequences_ids)
images = images.flatten()
unique_values = np.unique(images)
print(f'# of unique values: {len(unique_values)}\nUnique values: {unique_values}')
fig, axis = plt.subplots(1, 1)
axis.hist(images)
axis.set_title("Distribution of pixels")
plt.savefig("pixels_distribution.png")
'''
Is there a different in the illumination between the satellites and the other elements?
'''

'''
Can we convert the data into binary? (increase the SNR ratio)
'''
