# Description
The objective here is to operate on the whole image. I started first with a naive RF, it performs poorly. I can start to *guess* why. I think  we need kernels in order to extract good features. I need to capture the spatial aspect of the image. I can see this issue repeating itself when using a MLP: it is simply blind! A bright spot can be either part of a star, or a valid object, or something else in the background. I need to filter the image, and maybe many times with multiple filters, before I can say I have a good set of features, valid for this task. 

This of course leads me to: convolution networks. However, there is big annoying issue: I don't have the computing power to effectively explore this domain, and the data is too large, that it is not clear how to upload it to my google drive yet (in order to use Google Colab). That really leaves me with little room for maneuver. I certainly believe that deploying kernels and filters is essential for any successful solution at the moment. 

So, what can be done?
  * Can I get free computing power from Azure for example? To use it to explore this problem?
  * Is there a smarter/easier way to upload the data to my google drive, without using the GUI? (because it usually fails for large files)
    * I think I can download the data again from scratch, directly on my google drive, using google colab! (the *wget* command), then unzip it there, and use it from there.
      * I should not put high hopes here. Maybe I don't have enough ram memory to load the data!!
      * So far, it **seems to work** :)) It is downloading now.
    * Amazon AWS spot-instance
  * Quality Diversity Search: what I need to collect unique/novel filters (I need a way to measure that, either from the filter parameters, or the resulting activations). Then, I can build another optimization layer to determine which filters to combine, in order to produce a feature map, that will be used to train a classifier. A good collection of filters should maximize the quality of classification.

---
# Experiment follow up

## Convolution network
### Step 1: basic exploration (setup the environment)
#### Objectives
- [X] Download the data to google colab
- [X] Run a basic network
- [ ] Save the pre-processed data, so that I don't need to load the data again file-by-file
  - **FAILED**. No enough memory
#### Observations
- The model overfits very quickly. Regularization is apparently needed
  - I don't remember much about how to regularize a model to be honest. I need to review this (and write a summary and experimentation setup about it)
- It is safe to assume that the current learning rate $\alpha = 0.001$ is okay, since the training data seems to optimize well.


# Conclusions

# What is next?
