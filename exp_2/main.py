import preprocessing
import classify_image
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report, cohen_kappa_score
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
import json 
import multiprocessing
import tqdm 
from numba import jit
from multiprocessing import Pool
import numpy as np  
import itertools
import validation
import pickle
# Parameters
RADIUS = 3
# RADIUS = 7
PREPARE_SUBMISSION = False
FULL_SUBMISSION = True
TRAIN_DATA_ID = range(1, 1281)
# TRAIN_DATA_ID = range(1, 1000)
# TRAIN_DATA_ID = range(1, 100)
VAL_DATA_PERCENTAGE = 0.1
TEST_DATA = range(1, 5121)
# TEST_DATA = range(1, 20)
FRAMES = range(1, 6)
LOAD_PICKLE = False
if __name__ == "__main__":
    # Load the data
    if LOAD_PICKLE:
        train_X, train_y = pickle.load(open("imagesXclasses.pkl", 'rb'))
        train_annotation = preprocessing.PreProcessing.read_annotation_file()
    else:
        train_X, train_y, train_annotation = preprocessing.generate_dataset(sequence_id_list=TRAIN_DATA_ID)
        pickle.dump([train_X, train_y], open("imagesXclasses.pkl", 'wb'), protocol=pickle.HIGHEST_PROTOCOL)

    # Split the data into training and validation
    train_X, val_X, train_y, val_y = train_test_split(
        train_X, train_y, test_size=VAL_DATA_PERCENTAGE, shuffle=True, stratify=train_y)

    print(val_y)
    # Train the classifier
    train_X = train_X.reshape(train_X.shape[0], -1)
    val_X = val_X.reshape(val_X.shape[0], -1)
    # classifier_model = RandomForestClassifier(n_estimators=500, max_depth=30, n_jobs=-1, class_weight="balanced_subsample")
    # classifier_model = MLPClassifier(hidden_layer_sizes=(200,), max_iter=1000, verbose=True, learning_rate='adaptive', early_stopping=True)
    classifier_model = RandomForestClassifier(n_estimators=1000, max_depth=30, n_jobs=-1, verbose=1)

    classifier_model.fit(train_X, train_y)

    pred_labels = classifier_model.predict(val_X)
    print(classification_report(pred_labels, val_y))
    print(confusion_matrix(pred_labels, val_y))
    print("Kappa =", cohen_kappa_score(pred_labels, val_y))

    #exit()
    submission = {}
    ground_truth = []
    for sequence_id in tqdm.tqdm(val_data_id, desc='Sequence Loop'):
        print('Preparing the validation data for evaluation')
        submission[sequence_id] = {}
        for frame_id in tqdm.tqdm(FRAMES, desc='Frame Loop'):
            sub_list = classify_image.predict_objects(path='../data/train', sequence_id=sequence_id, frame_id=frame_id, model=classifier_model, radius=RADIUS, max_size=1) # I am not sure about what the max size here means
            # print(f'seq: {sequence_id} - frame_id: {frame_id} --> {submission}')
            # print(sub_list)
            submission[sequence_id][frame_id] = sub_list
            # print(f'score_frame: ', validation.score_frame(sub_list, val_annotation[sequence_id][frame_id]))
        TP, FN, FP, mse = validation.score_sequence(submission[sequence_id], val_annotation[sequence_id])
        print(f'TP: {TP}, FN: {FN}, FP: {FP}, mse: {mse}')
    
