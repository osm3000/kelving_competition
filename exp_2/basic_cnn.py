'''
Here, I start a basic experiment with CNN. I am convinced that having spatial information is the key to a good classifier (and later, a regressor).
'''
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
import matplotlib.pyplot as plt
import preprocessing
import pickle
from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Change the numerical computation from float32 to mixed_float16
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_policy(policy)

DIAG_MODE = False
# Load the data
train_X, train_y = pickle.load(open("imagesXclasses.pkl", 'rb'))
if DIAG_MODE:
    train_X = train_X[0:200]
    train_y = train_y[0:200]
# Filter the data that has more than 4 classes
valid_items = train_y<=3
# train_X = train_X[valid_items].astype(np.float32)
# train_y = train_y[valid_items].astype(np.int16)
train_X = train_X[valid_items]
train_y = train_y[valid_items]

# Split the data into train and val
train_X, val_X, train_y, val_y = train_test_split(train_X, train_y, test_size=0.1, shuffle=True, stratify=train_y)

# pickle.dump([train_X, val_X, train_y, val_y], open("reduced_imagesXclasses.pkl", 'wb'), protocol=pickle.HIGHEST_PROTOCOL)
# exit()
# Put the data in correct dimensions, needed for the model
train_X = np.expand_dims(train_X, axis=-1)
train_y = np.expand_dims(train_y, axis=-1)

val_X = np.expand_dims(val_X, axis=-1)
val_y = np.expand_dims(val_y, axis=-1)

input_shape = train_X.shape[1:]
train_y = keras.utils.to_categorical(train_y)
val_y = keras.utils.to_categorical(val_y)

np_classes = train_y.shape[1]

print(f'train_X.shape: {train_X.shape} -- train_y.shape: {train_y.shape}')
print(f'val_X.shape: {val_X.shape} -- val_y.shape: {val_y.shape}')

model = keras.Sequential(
    [
        keras.Input(shape=input_shape),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Flatten(),
        layers.Dropout(0.5),
        layers.Dense(np_classes, dtype='float32', activation="softmax"),
    ]
)

print(model.summary())

my_callbacks = [
    keras.callbacks.EarlyStopping(patience=5)
]

batch_size = 16
epochs = 200
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=[
              "accuracy", keras.metrics.CategoricalAccuracy()])

model.fit(train_X, train_y, batch_size=batch_size, epochs=epochs, callbacks=my_callbacks, use_multiprocessing=False, validation_data=(val_X, val_y))

# Evaluate the model
# score = model.evaluate(x_test, y_test, verbose=0)
# print("Test loss:", score[0])
# print("Test accuracy:", score[1])
