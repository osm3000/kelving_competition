from skimage import measure
import random 
import itertools
import json
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
import pickle
# %matplotlib inline

random.seed(0)

class PreProcessing():
    def __init__(self):
        pass

    @staticmethod
    def read_image(path):
        return plt.imread(path)

    @staticmethod
    def generate_labeled_data(image_path, annotation):
        im_array = PreProcessing.read_image(image_path)
        nb_objects = len(annotation)
        return nb_objects, im_array

    @staticmethod
    def generate_labeled_set(annotation_array, path, sequence_id_list):
        # Generate labeled data for a list of sequences in a given path
        labels = []
        # labels = np.zeros((len(sequence_id_list)*6))
        # features = np.zeros((len(sequence_id_list)*6, 480, 640))
        # print(f'features: {features.shape} -- labels: {labels.shape}')
        features = []
        global_index = 0
        for seq_index, seq_id in enumerate(sequence_id_list):
            # print(f'Seq id: {seq_id}')
            for frame_id in range(1, 6):
                d = PreProcessing.generate_labeled_data(f"{path}{seq_id}/{frame_id}.png", annotation_array[seq_id][frame_id])
                #labels[global_index] = d[0]
                # features[global_index, :, :] = d[1]
                labels.append(d[0])
                features.append(d[1].astype(np.float16))
                global_index += 1
                # features.append(d[1])
        # features = features.reshape(-1, 480, 640)
        return np.array(labels).astype(np.int8), np.stack(features)

    @staticmethod
    def read_annotation_file(path='../spotGEO/train_anno.json'):
        print(f'path: {path}')
        with open(path) as annotation_file:
            annotation_list = json.load(annotation_file)
        # Transform list of annotations into dictionary
        annotation_dict = {}
        for annotation in annotation_list:
            sequence_id = annotation['sequence_id']
            if sequence_id not in annotation_dict:
                annotation_dict[sequence_id] = {}
            annotation_dict[sequence_id][annotation['frame']] = annotation['object_coords']
        return annotation_dict


def generate_dataset(sequence_id_list=range(1, 101), path="../data"):
    train_annotation = PreProcessing.read_annotation_file(f'{path}/train_anno.json')

    train_labels, train_features = PreProcessing.generate_labeled_set(
        annotation_array=train_annotation, path=f'{path}/train/', sequence_id_list=sequence_id_list)
    print(f'train_features: {train_features.shape} -- train_labels: {train_labels.shape}')
    return train_features, train_labels, train_annotation
