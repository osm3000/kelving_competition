'''
Here, I start a basic experiment with CNN. I am convinced that having spatial information is the key to a good classifier (and later, a regressor).
'''
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
import matplotlib.pyplot as plt
import preprocessing
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, classification_report, cohen_kappa_score

DIAG_MODE = False
# Load the data
train_X, val_X, train_y, val_y = pickle.load(open("reduced_imagesXclasses.pkl", 'rb'))

# Put the data in correct dimensions, needed for the model
train_X = train_X.reshape(train_X.shape[0], -1)
val_X = val_X.reshape(val_X.shape[0], -1)

print(f'train_X.shape: {train_X.shape} -- train_y.shape: {train_y.shape}')
print(f'val_X.shape: {val_X.shape} -- val_y.shape: {val_y.shape}')

classifier_model = RandomForestClassifier(
    n_estimators=10, n_jobs=-1, verbose=1)

classifier_model.fit(train_X, train_y)

print("-"*20)
print("Training data report")
pred_labels = classifier_model.predict(train_X)
print(classification_report(pred_labels, train_y))
print(confusion_matrix(pred_labels, train_y))
print("Kappa =", cohen_kappa_score(pred_labels, train_y))

print("-"*20)
print("Validation data report")
pred_labels = classifier_model.predict(val_X)
print(classification_report(pred_labels, val_y))
print(confusion_matrix(pred_labels, val_y))
print("Kappa =", cohen_kappa_score(pred_labels, val_y))
