import preprocessing
import classify_image
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report, cohen_kappa_score
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
import json 
import multiprocessing
import tqdm 
from numba import jit
from multiprocessing import Pool
import numpy as np
import itertools
import validation
# Parameters
RADIUS = 3
# RADIUS = 7
PREPARE_SUBMISSION = False
FULL_SUBMISSION = True
TRAIN_DATA_ID = range(1, 1281)
# TRAIN_DATA_ID = range(1, 100)
VAL_DATA_PERCENTAGE = 0.1
TEST_DATA = range(1, 5121)
# TEST_DATA = range(1, 20)
FRAMES = range(1, 6)
if __name__ == "__main__":
    # Split the data into training and validation
    train_data_id, val_data_id = train_test_split(TRAIN_DATA_ID, test_size=VAL_DATA_PERCENTAGE)
    # print(train_data_id, val_data_id)
    # exit()
    # Load the data
    train_X, train_y, train_annotation = preprocessing.generate_dataset(sequence_id_list=train_data_id, radius=RADIUS, nb_false=200)
    val_X, val_y, val_annotation = preprocessing.generate_dataset(sequence_id_list=val_data_id, radius=RADIUS, nb_false=200)
    # print(val_annotation)
    # exit()
    # train_X, train_y = preprocessing.generate_dataset(sequence_id_list=range(1, 200), radius=RADIUS, nb_false=20)
    
    # train_X, val_X, train_y, val_y = train_test_split(train_X, train_y, test_size=0.1, stratify=train_y)

    # Train the classifier
    classifier_model = RandomForestClassifier(n_estimators=500, max_depth=30, n_jobs=-1, class_weight="balanced_subsample")
    classifier_model.fit(train_X, train_y)

    pred_labels = classifier_model.predict(val_X)
    print(classification_report(pred_labels, val_y))
    print(confusion_matrix(pred_labels, val_y))
    print("Kappa =", cohen_kappa_score(pred_labels, val_y))


    # Evaluate the quality of the validation
    # for sequence_id in tqdm.tqdm(val_data_id, desc='Sequence Loop'):
    #     submission = []
    #     ground_truth = []
    #     print('Preparing the validation data for evaluation')
    #     for frame_id in tqdm.tqdm(FRAMES, desc='Frame Loop'):
    #         sub_list = classify_image.predict_objects(path='../spotGEO/train', sequence_id=sequence_id, frame_id=frame_id, model=classifier_model, radius=RADIUS, max_size=1) # I am not sure about what the max size here means
    #         submission.append({"sequence_id": sequence_id,
    #                             "frame": frame_id,
    #                             "num_objects": len(sub_list),
    #                             "object_coords": sub_list})
            
    #         ground_truth.append({"sequence_id": sequence_id,
    #                 "frame": frame_id,
    #                 "num_objects": len(val_annotation[sequence_id][frame_id]),
    #                 "object_coords": val_annotation[sequence_id][frame_id]})
            
        
    # with open('val_data_predicted.json', 'w') as outfile:
    #     json.dump(submission, outfile)
    # with open('val_data_groundtruth.json', 'w') as outfile:
    #     json.dump(ground_truth, outfile)
    submission = {}
    ground_truth = []
    for sequence_id in tqdm.tqdm(val_data_id, desc='Sequence Loop'):
        print('Preparing the validation data for evaluation')
        submission[sequence_id] = {}
        for frame_id in tqdm.tqdm(FRAMES, desc='Frame Loop'):
            sub_list = classify_image.predict_objects(path='../spotGEO/train', sequence_id=sequence_id, frame_id=frame_id, model=classifier_model, radius=RADIUS, max_size=1) # I am not sure about what the max size here means
            # print(f'seq: {sequence_id} - frame_id: {frame_id} --> {submission}')
            # print(sub_list)
            submission[sequence_id][frame_id] = sub_list
            # print(f'score_frame: ', validation.score_frame(sub_list, val_annotation[sequence_id][frame_id]))
        TP, FN, FP, mse = validation.score_sequence(submission[sequence_id], val_annotation[sequence_id])
        print(f'TP: {TP}, FN: {FN}, FP: {FP}, mse: {mse}')
    
    # print(f'keys: {list(submission.keys())}')
    # print(f'keys: {list(val_annotation.keys())}')
    # for key in submission:
    #     print(f'seq: {key} ------- {list(submission[key].keys())}')
    #     print(f'seq: {key} ------- {list(val_annotation[key].keys())}')
    #     print("----------------------------------------------------------------------------")
    # with open('valdata_predicted.json', 'w') as fileHandle:
    #     json.dump(submission, outfile)
    # with open('valdata_groundtruth.json', 'w') as fileHandle:
    #     json.dump(val_annotation, outfile)

    print(f'score sequences: ', validation.score_sequences(submission, val_annotation))
        
    if PREPARE_SUBMISSION:
        if not FULL_SUBMISSION:
            submission = []
            print("Submission process begin")
            for sequence_id in tqdm.tqdm(TEST_DATA, desc='Sequence Loop'):
                for frame_id in tqdm.tqdm(FRAMES, desc='Frame Loop'):
                    if sequence_id < 100: # for the sake of testing, i am skipping most of the frames, but it should cover the whole test set
                        sub_list = classify_image.predict_objects(
                            sequence_id=sequence_id, frame_id=frame_id, model=classifier_model, radius=RADIUS, max_size=1) # I am not sure about what the max size here means
                        submission.append({"sequence_id": sequence_id,
                                        "frame": frame_id,
                                        "num_objects": len(sub_list),
                                        "object_coords": sub_list})
                    else:
                        submission.append({"sequence_id": sequence_id,
                                        "frame": frame_id,
                                        "num_objects": 0,
                                        "object_coords": []})

            with open('my_submission.json', 'w') as outfile:
                json.dump(submission, outfile)
        else:
            nb_procs = 50
            p = Pool(processes=nb_procs)
            sequence_list, frame_list = np.arange(1, 5121), np.arange(1, 6)
            id_pair_list = list(itertools.product(sequence_list, frame_list))
            sub_sequence = p.starmap(classify_image.predict_objects, [(
                id_pair[0], id_pair[1], classifier_model, RADIUS, 1) for id_pair in id_pair_list])
            p.close()

            sub_dict = {id_pair: sub for id_pair,
                        sub in zip(id_pair_list, sub_sequence)}

            submission = []
            for id_pair, sub_list in sub_dict.items():
                submission.append({"sequence_id": int(id_pair[0]),
                                "frame": int(id_pair[1]),
                                "num_objects": len(sub_list),
                                "object_coords": sub_list})
            with open('my_submission.json', 'w') as outfile:
                json.dump(submission, outfile)

    # sequence_id, frame_id = 102, 1
    # target_image = plt.imread(f"../spotGEO/train/{sequence_id}/{frame_id}.png")
    # pred_image = classify_image.classify_image(
    #     im=target_image, model=classifier_model, radius=RADIUS)

    # plt.figure(figsize=(15, 10))
    # plt.imshow(pred_image, interpolation='None')
    # plt.axis('off')
    # plt.show()
