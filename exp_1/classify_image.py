import numpy as np
from preprocessing import PreProcessing
from collections import defaultdict
import matplotlib.pyplot as plt
from skimage import measure
from numba import jit
import matplotlib.pyplot as plt

def classify_image(im:np.ndarray, model, radius:int):
    n_features = (2*radius+1)**2  # Total number of pixels in the neighborhood
    feat_array = np.zeros((im.shape[0], im.shape[1], n_features))
    # Here, the loop over each pixel. I am guessing this leads to a lot of false detection. Let's relax it, by doing non-overlapping windows
    for x in range(radius+1, im.shape[0]-(radius+1)):
        for y in range(radius+1, im.shape[1]-(radius+1)):
            feat_array[x, y, :] = PreProcessing.extract_neighborhood(
                x, y, im, radius)
    # for x in range(radius+1, im.shape[0]-(radius+1), radius):
    #     for y in range(radius+1, im.shape[1]-(radius+1), radius):
    #         feat_array[x:x+radius, y:y+radius, :] = PreProcessing.extract_neighborhood(
    #             x, y, im, radius)
    all_pixels = feat_array.reshape(im.shape[0]*im.shape[1], n_features)
    pred_pixels = model.predict(all_pixels).astype(np.bool_)
    pred_image = pred_pixels.reshape(im.shape[0], im.shape[1])
    # fig, axis = plt.subplots(1, 2)
    # axis = axis.flatten()
    # axis[0].imshow(im)
    # axis[1].imshow(pred_image)
    # plt.show()
    return pred_image


def extract_centroids(pred, bg=0):
    '''
    We want to extract the connected segments from the predicted image (all the neighboring parts that are classified as 1)
    '''
    conn_comp = measure.label(pred, background=bg)
    # conn_comp = measure.label(pred, connectivity=2, background=bg)
    # Keys are the indices of the connected components and values are arrrays of their pixel coordinates
    object_dict = defaultdict(list)
    for (x, y), label in np.ndenumerate(conn_comp):
        if label != bg:
            object_dict[label].append([x, y])
    # Mean coordinate vector for each object, except the "0" label which is the background
    centroids = {label: np.mean(np.stack(coords), axis=0)
                 for label, coords in object_dict.items()}
    object_sizes = {label: len(coords)
                    for label, coords in object_dict.items()}
    # print(f'centroids: {centroids}')
    # print(f'object_sizes: {object_sizes}')
    return centroids, object_sizes


def filter_large_objects(centroids, object_sizes, max_size):
    '''
    We can try to filter out the larger objects which are obviously not viable candidates.
    '''
    small_centroids = {}
    for label, coords in centroids.items():
        if object_sizes[label] <= max_size:
            small_centroids[label] = coords
    return small_centroids


def predict_objects(sequence_id, frame_id, model, radius, max_size, path='../spotGEO/test'):
    '''
    First we will run the whole classification+filtering process on one frame of the test set.
    '''
    test_image = plt.imread(f"{path}/{sequence_id}/{frame_id}.png")
    test_pred = classify_image(test_image, model, radius)
    test_centroids, test_sizes = extract_centroids(test_pred, 0)
    test_centroids = filter_large_objects(test_centroids, test_sizes, max_size)
    # Switch x and y coordinates for submission
    if len(test_centroids.values()) > 0:
        sub = np.concatenate([c[np.array([1, 0])].reshape((1, 2))
                              for c in test_centroids.values()])
        #np array converted to list for json serialization, truncated to the first 30 elements
        return sub.tolist()[0:30]
    else:
        return []
