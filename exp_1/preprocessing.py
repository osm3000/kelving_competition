from skimage import measure
import random 
import itertools
import json
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
import pickle
# %matplotlib inline

random.seed(0)

class PreProcessing():
    def __init__(self):
        pass

    @staticmethod
    def read_image(path):
        return plt.imread(path)
    
    @staticmethod
    def random_different_coordinates(coords, size_x, size_y, pad):
        """ Returns a random set of coordinates that is different from the provided coordinates, 
        within the specified bounds.
        The pad parameter avoids coordinates near the bounds."""
        good = False
        while not good:
            good = True
            c1 = random.randint(pad + 1, size_x - (pad + 1))
            c2 = random.randint(pad + 1, size_y - (pad + 1))
            for c in coords:
                if c1 == c[0] and c2 == c[1]:
                    good = False
                    break
        return (c1, c2)

    @staticmethod
    def extract_neighborhood(x, y, arr, radius):
        """ Returns a 1-d array of the values within a radius of the x,y coordinates given """
        return arr[(x - radius): (x + radius + 1), (y - radius): (y + radius + 1)].ravel()

    @staticmethod
    def check_coordinate_validity(x, y, size_x, size_y, pad):
        """ Check if a coordinate is not too close to the image edge """
        return x >= pad and y >= pad and x + pad < size_x and y + pad < size_y

    @staticmethod
    def generate_labeled_data(image_path, annotation, nb_false, radius):
        """ For one frame and one annotation array, returns a list of labels 
        (1 for true object and 0 for false) and the corresponding features as an array.
        nb_false controls the number of false samples
        radius defines the size of the sliding window (e.g. radius of 1 gives a 3x3 window)"""
        features, labels = [], []
        im_array = PreProcessing.read_image(image_path)
        # True samples
        for obj in annotation:
            # Project the floating coordinate values onto integer pixel coordinates.
            obj = [int(x + .5) for x in obj]
            # For some reason the order of coordinates is inverted in the annotation files
            if PreProcessing.check_coordinate_validity(obj[1], obj[0], im_array.shape[0], im_array.shape[1], radius):
                features.append(PreProcessing.extract_neighborhood(
                    obj[1], obj[0], im_array, radius))
                labels.append(1)
        
        for i in range(nb_false):
            c = PreProcessing.random_different_coordinates(
                annotation, im_array.shape[1], im_array.shape[0], radius)
            features.append(PreProcessing.extract_neighborhood(
                c[1], c[0], im_array, radius))
            labels.append(0)
        # print(labels, features, len(annotation))
        return np.array(labels), np.stack(features, axis=1)

    @staticmethod
    def generate_labeled_set(annotation_array, path, sequence_id_list, radius, nb_false):
        # Generate labeled data for a list of sequences in a given path
        labels, features = [], []
        for seq_id in sequence_id_list:
            for frame_id in range(1, 6):
                # print(len(annotation_array[seq_id][frame_id]))
                if (nb_false == 0) and (len(annotation_array[seq_id][frame_id]) == 0):# there is nothing to get from this frame
                    continue
                else: 
                    try:
                        d = PreProcessing.generate_labeled_data(f"{path}{seq_id}/{frame_id}.png", annotation_array[seq_id][frame_id], nb_false, radius)
                        labels.append(d[0])
                        features.append(d[1]) 
                    except Exception as e:
                        print(f'ERROR - Fn: generate_labeled_set - {e}')
        return np.concatenate(labels, axis=0), np.transpose(np.concatenate(features, axis=1))

    @staticmethod
    def read_annotation_file(path):
        print(f'path: {path}')
        with open(path) as annotation_file:
            annotation_list = json.load(annotation_file)
        # Transform list of annotations into dictionary
        annotation_dict = {}
        for annotation in annotation_list:
            sequence_id = annotation['sequence_id']
            if sequence_id not in annotation_dict:
                annotation_dict[sequence_id] = {}
            annotation_dict[sequence_id][annotation['frame']] = annotation['object_coords']
        return annotation_dict


def generate_dataset(sequence_id_list=range(1, 101), radius=7, nb_false=10):
    train_annotation = PreProcessing.read_annotation_file('../data/train_anno.json')

    train_labels, train_features = PreProcessing.generate_labeled_set(
        annotation_array=train_annotation, path='../data/train/', sequence_id_list=sequence_id_list, radius=radius, nb_false=nb_false)
    print(f'train_features: {train_features.shape} -- train_labels: {train_labels.shape}')
    return train_features, train_labels, train_annotation
