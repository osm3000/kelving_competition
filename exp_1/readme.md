# Description
This is directly based on the code from the started kit. I want to experiment, and familiarize myself with the whole pipeline
The objective is to make a full submission now, with whatever score it is...

# TODO:
-[ ] For some reason, while the radius is 7, the features dimension is 225. Debug the issue
   * Solved. It was a misunderstanding from my side about how it works
-[ ] Parallelize the process of creating the submission file. This is too slow
   * Processing and submitting the whole test data is simply not feasible, especially with the current methodology (of classifying windows of the frame, one pixel shift at a time).
-[ ] Add numpa jit decorators to the different functions using numpy
   * Quite difficult to adapt the code and make it compatible. Focus on multiprocessing. Besides, there are more pressing issues at the moment.

# Conclusions
* I managed to submit finally. It is quite bad - worse than anything else submitted on the website
* I need to implement the evaluation metrics myself in order to enhance the quality of offline testing protocol.  
  * They are already implemented in *validation.py* file. I finally managed to adapt the functions there to my current format. 
* Processing all the test data is simple not feasible time wise
  * This is probably because of the methods I am using
  * Should not be excluded as a possibility for the future
* With the evaluation metrics in place, one noticeable thing is the the number of false positives is staggering, even though the quality of prediction by the classifier seems quite good most of the time. I am not sure why this behavior. 
  * Is the filtering step after classification the problem? Let's visualize it!
  * Is there a number in the classification metrics that doesn't make sense to me?
  * Should I change the way I am labelling the windows for the classification? So far, the strategy is if the star is in the window or not.
  * Do I need to generate a lot of false examples for the classifier during the training phase?
  * One of the probable reasons why there is a big discrepancy between the classifier performance and the final image evaluation is probably because I am using the 'wrong' false data. Think about it: the false data is sampled in a random way from the frame. Most of the frame, however, is easy to detect! It is just a black background!! So, compared to the detected satellites, it is a pretty easy job. However, in application for the whole image (hard reality), the existence of the stars is pretty confusing for the system. I know we filter them out after classification, but still, the filtering system is not good enough so far (I simply assume that big clusters are the problem, but the truth is, I am still detecting a huge amount of small objects).
    * Also, the positive examples maybe misleading as well. In the training, the windows are extracted in a way that they are fixed on the star (the star is in the center). However, in testing, this is not the case. We just move pixel by pixel.
      * Although, now that I wrote that, maybe the current approach is a not a bad one after all. 
    * Can I threshold the image (make it binary) so it has less noise? Loop over the data, see if the satellites have a particular min level of illumination.
* From what I see ![the distribution of objects](nb_objects_dist.png), we can see that 10 is the max number, and most of the objects is between 1 and 4. I think I can start from here. Let's try to classify the whole image, and use regression to predict the position of the different satellites. 
  * It is not clear for me how to do this in sklearn. Thus, I think we can create two models here, a classifier and a regressor. The classifier will tell us the number of objects in the image, and the regressor will then be triggered to tell us the places of those objects. In a sense, the classifier will generate a mask on the regressor output (we only care about certain number of outputs).
    * This can be experiment two!!
    * This is a more straightforward task using neural networks, where I can make one network for both, mix multiple loss function, and generate the mask on the fly. But right now, I want a proof of concept. 
    * The classifier will be trained on all the images, while the regressor will be trained on the good images only (the regressor will be triggered only if the classifier says it should be).
    * Let's test the classifier performance for now, as it is super important.

# What is next?
* I think I should dive into object recognition concepts in deep learning especially. 
* Read the papers that are referenced on the website.
* Do I really want to do this? Do I have the computational budget to do it?
* No need to move further on the starter kit I believe. It is a dead-end
* I should adapt FastAI strategy. The whole testing operation for a new idea should not be more than 10 min. Otherwise, this is a huge waste. I need to move quickly.
* 