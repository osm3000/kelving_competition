'''
Implement evaluation metrics, for better estimation of the performance of my algorithms. Relying on the server submission for evaluation is too expensive and limited.
How to evaluate my evaluation? I need to post this on the forum. Also, I am not clear on the values of taw and epsilon.
'''
# Status: cancelled. I just found out that it was already implemented and provided by the organization people.
import numpy as np
from scipy.optimize import linear_sum_assignment
EPSILON = 0.01
TAW = 0.1
L = 20000
def evaluate_one_frame(ground_truth: np.ndarray, predicted: np.ndarray):
    '''
    Different cases:
        - len(ground_truth) = 0
        - len(predicted) = 0
        - len(predicted) > 0 and len(ground_truth) > 0
    '''
    tp = 0
    fp = 0
    fn = 0
    sse = 0
    N = ground_truth.shape[0]
    Mf = predicted.shape[0]
    if (N > 0) and (Mf > 0):
        distances_matrix = np.zeros((Mf, N))
        # Calculate the distances between objects
        for pre_index in range(Mf):
            for gt_index in range(N):
                distances_matrix[pre_index, gt_index] = np.sum(
                    (ground_truth[gt_index] - predicted[pre_index])**2)

        # Threshold the distance using the TAW value
        distances_matrix[distances_matrix > TAW] = L
        # assign different objects to each other
        # TODO: is the constraint mentioned in the document applied by default? or do I need to enforce them somehow?
        h_matrix = np.zeros_like(distances_matrix)
        h_matrix_ones = linear_sum_assignment(distances_matrix)
        h_matrix[h_matrix_ones] = 1
        h_dot_distance = h * distances_matrix

        # Calculate the tp, fn, fp
        for pre_index in range(Mf):
            for gt_index in range(N):
                # true positives
                if (h_matrix[pre_index, gt_index] == 1) and (distances_matrix[pre_index, gt_index] <= TAW):
                    tp += 1

                # false positives
                if pre_index == 0:
                    if (np.sum(h_dot_distance[:, gt_index]) == 0) or (h_dot_distance[:, gt_index] > TAW):
                        fp += 1

            if (np.sum(h_dot_distance[pre_index]) == 0) or (h_dot_distance[pre_index] > TAW):
                fn += 1
    elif (N == 0) or (Mf == 0):
        tp = 0
        fn = N
        fp = Mf

    return tp, fp, fn, sse

def eval_one_sequence(ground_truth: np.ndarray, predicted: np.ndarray):
    pass

def eval_dataset(ground_truth: np.ndarray, predicted: np.ndarray):
    pass
