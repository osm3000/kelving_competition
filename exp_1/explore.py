import json
import matplotlib.pyplot as plt
import preprocessing
import numpy as np
'''
What is the number of data points per frame?
'''
# path = '../spotGEO/train_anno.json' # path for the annotation file
# sizes = []
# with open(path) as annotation_file:
#     annotation_list = json.load(annotation_file)
#     # Transform list of annotations into dictionary
#     annotation_dict = {}
#     for annotation in annotation_list:
#         sequence_id = annotation['sequence_id']
#         if sequence_id not in annotation_dict:
#             annotation_dict[sequence_id] = {}
#         annotation_dict[sequence_id][annotation['frame']] = annotation['object_coords']
#         sizes.append(len(annotation['object_coords']))


# plt.figure()
# plt.title("Distribution of number of objects per frame")
# plt.hist(sizes)
# # plt.show()
# plt.savefig("nb_objects_dist.png")

'''
Explore the distribution of the pixels in the satellites
'''
# RADIUS = 1
# train_data_id = range(1, 1281)
# # train_data_id = np.random.choice(np.arange(1, 1281), size=(100))
# train_X, train_y, train_annotation = preprocessing.generate_dataset(sequence_id_list=train_data_id, radius=RADIUS, nb_false=0)
# print(train_X.shape)
# train_X = train_X.flatten()
# fig, axis = plt.subplots(1, 1)
# axis.hist(train_X)
# axis.set_title("Distribution of pixels for the satellites only")
# plt.savefig("pixels_distribution_satellites.png")

'''
Explore the distribution of the pixels in non-satellites
'''
RADIUS = 3
train_data_id = range(1, 1281)
# train_data_id = np.random.choice(np.arange(1, 1281), size=(100))
train_X, train_y, train_annotation = preprocessing.generate_dataset(sequence_id_list=train_data_id, radius=RADIUS, nb_false=100)
false_examples = train_y == 0
train_y = train_y[false_examples]
train_X = train_X[false_examples]
print(train_X.shape)
train_X = train_X.flatten()
fig, axis = plt.subplots(1, 1)
axis.hist(train_X)
axis.set_title("Distribution of pixels for the non-satellites")
plt.savefig("pixels_distribution_non_satellites.png")