'''
Efficient data loader. 
'''
import numpy as np
import preprocessing

class DataLoader(object):
    def __init__(self, data_path="../data/train/", annotation_path='../data/train_anno.json', **hwrgs):
        '''
        data_path: where the data is
        data_dict: the id of available data points for the loader
        '''
        super().__init__()
        self.data_path = data_path
        self.annotation_array = preprocessing.PreProcessing.read_annotation_file(path=annotation_path)
        self.scale_coordinates = hwrgs.get('scale_coord', True) # will scale the coordinates to be between 0 and 1
        self.sort_coord = hwrgs.get('sort_coord', True) # sort coordinates, according to how far they are from the point of origin
        self.padding = hwrgs.get('padding', True) # sort coordinates, according to how far they are from the point of origin
        self.max_nb_sats = hwrgs.get('max_nb_sats', 3)
        self.padding_value = hwrgs.get('padding_value', -1)

    def load_batch(self, batch_size:int, data_dict:np.ndarray):
        """
        Load batch size from the data. If batch size is larger than the available data, load all the data.
        """
        assert (batch_size > 0)
        if batch_size < len(data_dict):
            batch_indices = np.random.choice(data_dict.shape[0], size=batch_size, replace=False)
            batch_ids = data_dict[batch_indices]
        else:
            batch_ids = data_dict

        total_features = []
        total_nb_of_coordinates = []
        total_coordinates = []
        for item in batch_ids:
            seq_id, frame_id = item
            nb_of_coordinates, features = preprocessing.PreProcessing.generate_labeled_data(f"{self.data_path}{seq_id}/{frame_id}.png", self.annotation_array[seq_id][frame_id])
            coordinations = self.annotation_array[seq_id][frame_id]
            
            # Apply different pre-processing steps
            # 0. Sort the coordinates order for each frame
            if self.sort_coord:
                coordinations = preprocessing.PreProcessing.sort_coord(coordinations)
                print(coordinations)

            # 1. Scaling the coordinates to be between 0 and 1
            if self.scale_coordinates:
                for index, item in enumerate(coordinations):
                    coordinations[index][0] /= 640
                    coordinations[index][1] /= 480
            
            # 2. Padding the coordinates, to make them all of equal length.
            if self.padding:
                if nb_of_coordinates > self.max_nb_sats:
                    total_coordinates.append(coordinations)
                elif nb_of_coordinates < self.max_nb_sats:
                    total_coordinates.append(coordinations + [[self.padding_value, self.padding_value]] * (self.max_nb_sats - nb_of_coordinates))
                else:
                    total_coordinates.append(coordinations)

            total_features.append(features)
            total_nb_of_coordinates.append(nb_of_coordinates)

        return np.array(total_coordinates), np.array(total_nb_of_coordinates).astype(np.int8), np.expand_dims(np.stack(total_features), -1)

if __name__ == "__main__":
    demo_loader = DataLoader()
    data_dict = np.array([[1, 1], [1, 2], [2, 3], [2, 5], [3, 1]]) # (seq_id, frame_id) sequences
    my_batch = demo_loader.load_batch(batch_size = 4, data_dict = data_dict)

    for index, item in enumerate(my_batch):
        print(item.shape)