
class Fruit:
    def __init__(self):
        # super().__init__()
        print("Fruits here")

        
class Banana(Fruit):
    def __init__(self):
        super().__init__()
        print("Banana here")
    
    def create(self):
        print("BANANAS ----------------------->")

class Apple(Fruit):
    def __init__(self, name='Omar'):
        super().__init__()
        print(f"Apple here -- name: {name}")
    
    def create(self):
        print("Apples ----------------------->")

class Kiwi(Fruit):
    def __init__(self):
        super().__init__()
        print("Kiwi here")
    
    def create(self):
        print("Kiwi ----------------------->")

class Food(Kiwi, Apple, Banana):
    def __init__(self, name="Alaa"):
        # super().__init__()
        Apple.__init__(self, name=name)
        print("Food here")

if __name__ == "__main__":
    my_food = Food()
    my_food.create()