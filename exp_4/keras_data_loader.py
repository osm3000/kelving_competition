"""
This is an extension to the DataLoader class I built before, in order to fit with Keras paradigm
"""
import tensorflow.keras as keras
from data_loader import DataLoader

class DataGenerator(keras.utils.Sequence, DataLoader):
    'Generates data for Keras'
    # def __init__(self, list_IDs, labels, batch_size=32, dim=(32,32,32), n_channels=1, n_classes=10, shuffle=True):
    def __init__(self, batch_size=32, data_path="../data/train/", annotation_path='../data/train_anno.json', **hwrgs):
        'Initialization'
        DataLoader.__init__(self)
        self.on_epoch_end()
        
        self.my_data_loader = DataLoader()

    def __len__(self):
        'Denotes the number of batches per epoch'
        pass
    def __getitem__(self, index):
        'Generate one batch of data'
        pass

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        pass

    # def __data_generation(self, list_IDs_temp):
    #     'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
    #     # Initialization
    #     X = np.empty((self.batch_size, *self.dim, self.n_channels))
    #     y = np.empty((self.batch_size), dtype=int)

    #     # Generate data
    #     for i, ID in enumerate(list_IDs_temp):
    #         # Store sample
    #         X[i,] = np.load('data/' + ID + '.npy')

    #         # Store class
    #         y[i] = self.labels[ID]

    #     return X, keras.utils.to_categorical(y, num_classes=self.n_classes)