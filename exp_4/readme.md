# Objectives
This part, I will build a NN system to perform the following two tasks
- Detection: a classification task, to determine how many sats are there
- Localization: a regression task, to determine the location of each sats
This is most probably a convnet. Before anything serious in the design of this network, I should try transfer learning (using ResNet and bla bla) for example. Although I have low expectations for this to be of any use, it still worth the try.

# Questions
- Should the coordination of each sat be arranged, so that it goes from the closest to the point of origin, to the furthest? My point is that, if no order exists, and the first output of the neural network matches the 2nd output of the ground truth, it will still be gradeed as wrong. If no order exists, we want to grade each output of the NN with the nearest one in the ground truth. 
    - I think this point makes a good sense. The network should learn the order of the different stars.

# Comments
- Naive transfer learning (extract features from resnet50, use them to train a randomforest classifier to get the number of satellites). The results are shit!.
    - Maybe I should try a regression approach here instead. 
