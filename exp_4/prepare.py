"""
This will make a list of possible seq/frame that satisfies a particular criteria (mainly and only for now, the num of satellites)
"""
import preprocessing
from sklearn.model_selection import train_test_split
import numpy as np
import pickle
import json
# Define parameters 
MAX_SAT = 3

SERIAL_SIZE = 16 # When serialing to a file
if __name__ == "__main__":
    valid_data_points = []
    # Select only the data that has <= MAX_SAT
    annotations = preprocessing.PreProcessing.read_annotation_file()
    for seq_id in range(1, 1280+1):
        for frame_id in range(1, 5+1):
            if len(annotations[seq_id][frame_id]) <= MAX_SAT:
                valid_data_points.append((seq_id, frame_id))

    # Save the indices for the valid data points
    json.dump(valid_data_points, open('valid_data.json', 'w'))