from preprocessing import generate_dataset
import numpy as np
import matplotlib as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import numpy.ma as ma
import json
from data_loader import DataLoader

from tensorflow.keras.mixed_precision import experimental as mixed_precision
# Change the numerical computation from float32 to mixed_float16
policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_policy(policy)

# Parameters
VAL_RATIO = 0.1
NB_DATA_POINT = 10

if __name__ == "__main__":
    # Load data points dict
    valid_data_points = json.load(open('valid_data.json', 'r'))    
    np.random.shuffle(valid_data_points)
    if NB_DATA_POINT > 0: # In case I want a small part of the data, for testing reasons
        valid_data_points = valid_data_points[:NB_DATA_POINT]
    
    training_data, val_data = train_test_split(valid_data_points, test_size=VAL_RATIO)

    dataloader = DataLoader()
    # my_batch = demo_loader.load_batch(batch_size = 4, data_dict = data_dict)

    model = RandomForestRegressor(n_jobs=10, n_estimators=50, verbose=10, max_depth=30)
    
    model.fit(features_train, nb_of_sats_train)

    train_score = model.score(features_train, nb_of_sats_train)

    print(f'Train --> {train_score}')

    test_score = model.score(features_test, nb_of_sats_test)

    print(f'test --> {test_score}')
