from preprocessing import generate_dataset
import tensorflow.keras as keras
import tensorflow as tf
import pickle
import numpy as np

def simple_model(data_shape=(480, 640, 1), classification_outputs = 4, regression_outputs = 3*2)

# Experimenting with transfer learning here
if __name__ == "__main__":
    sats_coord, nb_of_sats, features = generate_dataset(sequence_id_list=range(1, 1200), path="../data", max_num_of_sats=3, padding_value=-1)
    features = np.expand_dims(features, -1)
    features = np.concatenate([features, features, features], axis=-1)
    # sats_coord = sats_coord.astype(np.int16)
    print(features.shape)

    model = keras.applications.ResNet50(include_top=False, weights="imagenet", input_shape=(480, 640, 3), pooling=None)
    # print(features.shape)
    with open('resnet_features.pkl', 'wb') as file_handle:
        index = 0
        step = 100
        while index < features.shape[0]:
            print(f'index: {index}')
            if index + step < features.shape[0]:
                predictions = model.predict(features[index:index+step])
            else:
                predictions = model.predict(features[index:])

            pickle.dump(predictions, file_handle, protocol=4)
            index += step
