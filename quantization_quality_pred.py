'''
I want to test the prediction quality for the learned quantization (on the reconstruction score) VS naive uniform distribution, for the different levels of optimization.
'''
import matplotlib.pyplot as plt
from exp_2 import preprocessing
import pandas as pd
import numpy as np
import pygmo as pg
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
import json
sequences_ids = np.random.choice(np.arange(1, 1281), size=500)
# sequences_ids = np.random.choice(np.arange(1, 1281), size=50)
features, labels, _ = preprocessing.generate_dataset(
    sequence_id_list=sequences_ids, path='./data')

labels_needed = labels <= 3
labels = labels[labels_needed]
features = features[labels_needed]
features = features.reshape(features.shape[0], -1)

quant_bins = {4: [0.62118242, 0.87452676, 0.12371975, 0.37741941],
              8: [0.43734035, 0.56829272, 0.69049424, 0.06413283, 0.95902683, 0.80402347, 0.3020223, 0.18546024],
              16: [0.53700116, 0.49183394, 0.8549489,  0.15362248, 0.3361867,  0.8930348, 0.59058275, 0.6350277,  0.02845493, 0.95089991, 0.08296628, 0.70566739, 0.28611832, 0.20939969, 0.78079028, 0.40046678]}

unifrom_results = {}
non_unifrom_results = {}
# for nb_levels in quant_bins:
for nb_levels in [0, 4, 8, 16, 32, 64, 128]:
# for nb_levels in [0, 4]:
    print("#-" * 20)
    print(f'# of levels: {nb_levels}')
    print("#-" * 20)
    unifrom_results[nb_levels] = {'small': [], 'large': []}
    non_unifrom_results[nb_levels] = {'small': [], 'large': []}
    for repetition in range(5):
    # for repetition in range(2):
        if nb_levels != 0: # Discrete 
            levels = np.linspace(0, 1, nb_levels)
            levels = np.sort(levels)
            features_new = np.digitize(features, levels) / nb_levels

            if repetition == 0:
                score = np.mean((features_new - features)**2)
                print(f'UNIFORM Reconstruction score: {score}')
                unifrom_results['reconst'] = score

            train_X, val_X, train_y, val_y = train_test_split(features_new, labels, test_size=0.1, shuffle=True, stratify=labels)
        else: # No quantization
            train_X, val_X, train_y, val_y = train_test_split(
                features, labels, test_size=0.1, shuffle=True, stratify=labels)

        classifier_model = RandomForestClassifier(n_estimators=10, n_jobs=8)
        classifier_model.fit(train_X, train_y)
        score = classifier_model.score(val_X, val_y)
        print(f'UNIFORM - SMALL MODEL prediction score: {score}')
        unifrom_results[nb_levels]['small'].append(score)

        classifier_model = RandomForestClassifier(n_estimators=100, n_jobs=8)
        classifier_model.fit(train_X, train_y)
        score = classifier_model.score(val_X, val_y)
        print(f'UNIFORM - LARGE MODEL prediction score: {score}')
        unifrom_results[nb_levels]['large'].append(score)

        if nb_levels != 0:
            del features_new
        del train_X
        del train_y
        del val_X
        del val_y
        del classifier_model

        if nb_levels in quant_bins: # Do the non-uniform ones
            levels = np.sort(quant_bins[nb_levels])
            features_new = np.digitize(features, levels) / nb_levels

            if repetition == 0:
                score = np.mean((features_new - features)**2)
                print(f'NON-UNIFORM Reconstruction score: {score}')
                non_unifrom_results['reconst'] = score

            train_X, val_X, train_y, val_y = train_test_split(features_new, labels, test_size=0.1, shuffle=True, stratify=labels)

            classifier_model = RandomForestClassifier(n_estimators=10, n_jobs=8)
            classifier_model.fit(train_X, train_y)
            score = classifier_model.score(val_X, val_y)
            print(f'NON-UNIFORM - SMALL MODEL prediction score: {score}')
            non_unifrom_results[nb_levels]['small'].append(score)

            classifier_model = RandomForestClassifier(n_estimators=100, n_jobs=8)
            classifier_model.fit(train_X, train_y)
            score = classifier_model.score(val_X, val_y)
            print(f'NON-UNIFORM - LARGE MODEL prediction score: {score}')
            non_unifrom_results[nb_levels]['large'].append(score)

            if nb_levels != 0:
                del features_new
            del train_X
            del train_y
            del val_X
            del val_y
            del classifier_model

json.dump({'uni': unifrom_results, 'non-uni': non_unifrom_results},
          open("results.json", 'w'))
