# General discussion
## Analysis of histograms of stars VS others VS general
Looking at the different histograms, we see the following:
* ![General distribution](./exp_2/pixels_distribution.png)
* ![Distribution of satellites](./exp_1/pixels_distribution_satellites.png)
* ![Distribution of non-satellites](./exp_1/pixels_distribution_non_satellites.png)

Those are taken under different sampling rates (hence, the difference in numbers), but the idea stands. The pixel illumination is not good enough to distinguish the satellites. This is probably why the classic methods (like RF and MLP) failed. From the pixels only, it is not possible to distinguish the satellites (which is the information fed to the classifier).

Also, it is not clear for me if a threshold can be applied here to the picture to reduce the information content in the image. Actually, a naive threshold could be harmful, since will deminish the distinguish between the satellites and the non-satellites. 
    
* Hmmm, but maybe there is a compromise here. We can just discard some of the information, but keep the other half. This will give more light on the details of that half.
    * Not really sure if this is a useful idea. 
    * Let's do what I did before: learn a good quantization, to reduce the number of values in the images (first, check what is the number of unique levels). To do that, give the quantized data to a classifier, and use its score to determine this non-linear quantization levels. This will make the best quantizers to serve the purpose of classifying the satellites.
        * First, check the number of unique levels in the data.
            * 254 levels, between 0 and 1.
        * Let's try to get 8, 16 and 32 levels at max --> this will help a lot reduce the amount of information in the images. 
    * We can do similar approach to build filters to remove the stars and other unnecessary items for example (not sure though what can this be).


---
* Maybe what I should do is to consider the whole sequence, instead of just single images. For the classification problem, from what I understand, the number of satellites should be the same all the time - even though in some frames it will not be visible - (re-check this point).
    * But, given that still, we know they are not clear in some frames (thus, not labelled). What is the impact in that case? I am not sure. I need to check.
* For neural networks, what is the situation now?
    * The images are super large, and with an increase in the size of the network. Compressing the images (or dealing with smaller subset of the images) is the key here. 
        * Maybe I can cut the image into smaller sections (similar to the code from starter kit). The raduis, however, I think it should be big enough in order to develop good filters. Let's say we divide the picture into 9 ones. What this will give me is: more data, but with smaller dimensions. I don't need to use all of this data, only part of it. I can also
    * Also, reduce the percision of the model. Right now, I think it is either float32 or float64. Something smaller, float16, will save precious resources.
    
---
Now that the quanitzation based on reconstruction error is done, I want to test its effect on the optimiz